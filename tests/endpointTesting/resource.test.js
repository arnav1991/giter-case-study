const request = require('supertest');
const app = require('../../app');
const jsonData = require('./resource.testdata.json');

describe('POST /resource/feature ', () => {
  jsonData.POST['/resource/feature'].cases.forEach((testCase) => {
      test(testCase.description, async () => {
        const response = await request(app).post('/resource/feature').send(testCase.request);
        if(testCase.response) {
          expect(response.body).toEqual(testCase.response);
        }
        if(testCase.statusCode) {
          expect(response.statusCode).toBe(testCase.statusCode);
        }
      });
  })
});
