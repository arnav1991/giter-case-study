const collectionDba = require('../db/dba/collection');

module.exports = {
  /**
   * AIM: This function is used to access data and any business logic will come here
   *
   * @param {string}  startDate   This is in format YYYY-MM-DD start date of the record (createdAt)
   * @param {string}  endDateDate   This is in format YYYY-MM-DD end date of the record (createdAt)
   * @param {int}  minCount   This is the minimum count of the sum of the records count (including this number)
   * @param {int}  maxCount   This is the maximum count of the sum of the records count (including this number)
   */
  findResourcesWithFilter: async ({ startDate, endDate, minCount, maxCount }) => {
    return collectionDba.fetchDocs({ startDate, endDate, minCount, maxCount });
  }
};
