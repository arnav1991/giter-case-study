module.exports = {
  findResourcesWithFilter: (data) => {
    if(Array.isArray(data)) {
        data.forEach((value, index) => {
            data[index]._id ? data[index]._id = undefined : null;
            data[index].value ? (data[index].value = undefined) : null;
            data[index].counts ? (data[index].counts = undefined) : null;
        });
    }
    return data;
  } 
};