var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const config = require('config');
const mongoose = require('mongoose');

var indexRouter = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

/**
 * Error Handler
 * Error handler all errors from all routes will come at this handler
 */

app.use(function (error, req, res, next) {
  if (process.env.NODE_ENV === 'localhost') {
    console.log(error);
  }
  let message = null;
  let status = config.get('status_code.INTERNAL_ERROR');
  if (error.errors) {
    return res.status(config.get('status_code.BAD_REQUEST')).send({
      code: config.get('status_code.BAD_REQUEST'),
      msg: error
    });
  }
  if (error instanceof Object) {
    if (error.message && error.status && error) {
      message = {
        message: error.message,
        stack: stringifyError(error)
      };
      status = error.status;
    } else
      message = {
        message: 'internal server error',
        stack: stringifyError(error)
      };
  } else if (typeof error == 'string' && !error.includes('Error')) {
    message = {
      message: error
    };
  } else {
    message = {
      message: 'internal server error',
      stack: stringifyError(error)
    };
  }
  return res.status(status).send({ code: status, msg: message });
});

const stringifyError = function (err, filter, space) {
  const plainObject = {};
  Object.getOwnPropertyNames(err).forEach(function (key) {
    plainObject[key] = err[key];
  });
  return JSON.stringify(plainObject, filter, space);
};

mongoose.connect(
  config.get('mongoUri'),
  {
    useNewUrlParser: true,
    retryWrites: false
  },
  (err) => {
    if (err) {
      console.log('Could not connect to MongoDB (DATA CENTER)');
      console.log(err);
    } else {
      console.log('DATA CENTER - Connected');
    }
  }
);

module.exports = app;
