const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Schema definition. 
 * Just showing the use case and process on how to use mongoose odm.
 */
const collectionSchema = new Schema(
  {
    key: { type: String },
    counts: { type: Array },
    createdAt: {
      type: Date,
      required: true,
      default: new Date()
    }
  },
  { strict: false }
);

module.exports = mongoose.model('records', collectionSchema);
