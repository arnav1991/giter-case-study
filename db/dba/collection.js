const collection = require('../models/collection');
const moment = require('moment');
const config = require('config');

module.exports = {
  /**
   * AIM: Fetch data from db using mongoose(orm).
   *
   * @param {string}  startDate   This is in format YYYY-MM-DD start date of the record (createdAt)
   * @param {string}  endDateDate   This is in format YYYY-MM-DD end date of the record (createdAt)
   * @param {int}  minCount   This is the minimum count of the sum of the records count (including this number)
   * @param {int}  maxCount   This is the maximum count of the sum of the records count (including this number)
   *
   * @note For the simplicity of the project data base managers have not been introduce in folder structure.
   */
  fetchDocs: ({ startDate, endDate, minCount, maxCount }) => {
    return collection.aggregate([
      { $addFields: { totalCount: { $sum: '$counts' } } },
      {
        $match: {
          createdAt: {
            $gte: moment(startDate, config.get('system_date_format')).toDate(),
            $lte: moment(endDate, config.get('system_date_format')).toDate()
          },
          totalCount: { $gte: minCount, $lte: maxCount }
        }
      }
    ]);
  }
};
