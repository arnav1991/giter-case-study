const validationError = require('../customErrorFormat');
const config = require('config');
const moment = require('moment');

/**
 * AIM these are the rules for schema validation of the APIs
 * Structure: Method -> Path
 */
module.exports = {
  POST: {
    feature: () => {
      /**
       * Types of validations
       * Date format through out the project should be YYYY-MM-DD and type String
       * startDate can never be greater than endDate
       * minCount and maxCount should be of type Integer
       * minCount can never be greater than maxCount
       */
      return {
        startDate: {
          in: 'body',
          exists: {
            errorMessage: 'startDate field is missing.'
          },
          isString: {
            errorMessage: 'startDate should be of type String'
          },
          custom: {
            options: (value) => {
              if (!moment(value, config.get(`system_date_format`)).isValid()) {
                throw new validationError(
                  `startDate: ${value} is not in proper format. Please use ${config.get(
                    `system_date_format`
                  )} formatted dates only`,
                  config.get('status_code.BAD_REQUEST')
                );
              }
              return true;
            }
          }
        },
        endDate: {
          in: 'body',
          exists: {
            errorMessage: 'endDate field is missing.'
          },
          isString: {
            errorMessage: 'endDate should be of type String'
          },
          custom: {
            options: (value, { req }) => {
              if (!moment(value, config.get(`system_date_format`)).isValid()) {
                throw new validationError(
                  `endDate: ${value} is not in proper format. Please use ${config.get(
                    `system_date_format`
                  )} formatted dates only`,
                  config.get('status_code.BAD_REQUEST')
                );
              }
              if (
                !moment(value, config.get(`system_date_format`)).isAfter(
                  moment(req.body.startDate, config.get(`system_date_format`))
                )
              ) {
                throw new validationError(
                  `endDate: ${value} should occur after startDate: ${req.body.startDate}`,
                  config.get('status_code.BAD_REQUEST')
                );
              }
              return true;
            }
          }
        },
        minCount: {
          in: 'body',
          exists: {
            errorMessage: 'minCount field is missing.'
          },
          isInt: {
            errorMessage: 'minCount should be of type Integer.'
          }
        },
        maxCount: {
          in: 'body',
          exists: {
            errorMessage: 'maxCount field is missing.'
          },
          isInt: {
            errorMessage: 'maxCount should be of type Integer.'
          },
          custom: {
            options: (value, { req }) => {
              if (value < req.body.minCount) {
                throw new validationError(
                  `minCount: ${req.body.minCount} should be less than maxCount: ${value}`,
                  config.get('status_code.BAD_REQUEST')
                );
              }
              return true;
            }
          }
        }
      };
    }
  }
};
