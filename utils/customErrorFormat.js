module.exports = class valdiationError extends Error {
  constructor(message, status) {
    super(message);
    this.status = status;
  }
};
