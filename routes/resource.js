const express = require('express');
const router = express.Router();
const { checkSchema, validationResult } = require('express-validator');
const validator = require('../utils/validators/resource');
const controller = require('../controllers/resource');
const config = require('config');
const serializer = require('../serializer/resource');

/**
 * AIM: This API is used to get records with filter.
 * Schema Validation of the request is done using express-validators.
 * Errors are handled by central error handler in app.js.
 * Serialization of the response is done through Serializer layer to meet the SLA.
 *
 * @param {string}  startDate   This is in format YYYY-MM-DD start date of the record (createdAt)
 * @param {string}  endDateDate   This is in format YYYY-MM-DD end date of the record (createdAt)
 * @param {int}  minCount   This is the minimum count of the sum of the records count (including this number)
 * @param {int}  maxCount   This is the maximum count of the sum of the records count (including this number)
 */
router.post('/feature', checkSchema(validator.POST.feature()), async (req, res, next) => {
  try {
    validationResult(req).throw();
    res.status(config.get('status_code.SUCCESS')).send({
      code: config.get('status_code.SUCCESS'),
      msg: 'success',
      records: serializer.findResourcesWithFilter(
        await controller.findResourcesWithFilter(req.body)
      )
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
