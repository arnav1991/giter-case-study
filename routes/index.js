const express = require('express');
const router = express.Router();
const resource = require('./resource');

// routing to the resource file
router.use('/resource', resource);

router.use('/health', (req, res) =>
  res.status(200).send({
    message: 'ok'
  })
);

module.exports = router;
